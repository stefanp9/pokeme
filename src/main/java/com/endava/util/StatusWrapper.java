/**
 * 
 */
package com.endava.util;

/**
 * @author Stefan.Petrovic
 *
 */
public class StatusWrapper {
	public static final String ONLINE = "online";
	public static final String OFFLINE = "offline";
	public static final String AWAY = "away";
	public static final String DO_NOT_DISTURB = "dnd";

}
