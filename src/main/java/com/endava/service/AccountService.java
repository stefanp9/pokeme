package com.endava.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.endava.dto.acount.AccountDto;
import com.endava.dto.acount.AccountRegistrationDto;
import com.endava.dto.contacts.ContactStatusResponseDto;
import com.endava.dto.contacts.ContactsResponseDto;
import com.endava.dto.login.LoginRequestDto;
import com.endava.dto.login.LoginResponseDto;
import com.endava.dto.login.LogoutRequestDto;
import com.endava.exceptions.EmailOccupiedException;
import com.endava.exceptions.InvalidCredentialsException;
import com.endava.model.Account;
import com.endava.repository.AccountRepository;
import com.endava.util.StatusWrapper;

@Service
public class AccountService {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private AccountRepository repository;
	
	public void createAccount(AccountRegistrationDto accountToCreate) throws EmailOccupiedException{
		System.out.println("uso sam");
		String email = accountToCreate.getEmail();
		Account account = repository.findByEmail(email);
		
		if(account != null){
			throw new EmailOccupiedException();
		} else {
			String password = accountToCreate.getPassword();
			accountToCreate.setPassword(passwordEncoder.encode(password));
			repository.save(new Account(accountToCreate));
		}
	}

	public AccountDto createUser(AccountDto userToCreate) {
		
		
		Account created = repository.save(new Account(userToCreate));

		return created.transferToDto();
	}

	public boolean deleteUser(Integer id) {
		Account user = repository.findOne(id);

		if (user != null) {
			repository.delete(user);
			return true;
		} else {
			System.out.println("User does not exist");
			return false;
		}
	}

	public AccountDto getUserById(Integer id) {
		AccountDto userDto = new AccountDto();

		Account user = repository.findOne(id);
		if (user != null) {
			userDto = user.transferToDto();
		}

		return userDto;
	}

	public List<AccountDto> getAll() {

		List<Account> users = repository.findAll();

		List<AccountDto> response = new ArrayList<AccountDto>();

		for (Account user : users) {
			response.add(user.transferToDto());
		}

		return response;
	}

	public void updateUser(AccountDto dto) {
		Account user = repository.findOne(dto.getId());

		if (user != null) {
			user.updateFromDto(dto);
			repository.save(user);
		}
	}

	public LoginResponseDto validateLoginCredentials(LoginRequestDto loginRegDto) throws InvalidCredentialsException {

		Account user = repository.findByEmail(loginRegDto.getEmail());

		if (user == null) {
			System.out.println("Nije ga nasao");
			throw new InvalidCredentialsException();
		} else {
			if (passwordEncoder.matches(loginRegDto.getPassword(), user.getPassword())) {
				user.setStatus(StatusWrapper.ONLINE);
				repository.save(user);
				return new LoginResponseDto("OK", user.getId());
			} else {
				throw new InvalidCredentialsException();
			}
		}
	}


	public void logout(Integer accountId) {
		Account user = repository.findOne(accountId);
		
		user.setStatus(StatusWrapper.OFFLINE);
		repository.save(user);
		
	}

	public List<ContactsResponseDto> getMyContacts(Integer id) {

		List<Account> accounts = repository.findByIdNot(id);
		List<ContactsResponseDto> myContats = new ArrayList<>();

		for (Account account : accounts) {

			myContats.add(account.transferToContactsResponseDto());

		}

		return myContats;
	}

	public List<ContactStatusResponseDto> getContactsStauts(Integer id) {

		List<Account> allUsers = repository.findByIdNot(id);
		List<ContactStatusResponseDto> statusList = new ArrayList<>();

		for (Account account : allUsers) {
			statusList.add(account.transferToContatcsStausDto());
		}
		
		return statusList;
		
	}

}
