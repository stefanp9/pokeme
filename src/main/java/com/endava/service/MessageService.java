/**
 * 
 */
package com.endava.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.endava.model.Message;
import com.endava.dto.message.MessageDto;
import com.endava.model.Account;
import com.endava.repository.MessageRepository;

import antlr.debug.MessageListener;

import com.endava.repository.AccountRepository;

/**
 * @author Stefan.Petrovic
 *
 */
@Service
public class MessageService {

	@Autowired
	private MessageRepository messageRepository;
	
	@Autowired
	private AccountRepository userRepository;
	
	// create ,essage
	public ResponseEntity<?> createMessage(MessageDto message){
		
		Account sender = userRepository.findOne(message.getSenderId());
		Account reciver = userRepository.findOne(message.getRecivedId());
		System.out.println(sender.toString()+ "" +reciver);
		
		
		if(sender == null || reciver == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}else
		{
			Message newMessage = new Message();
			
			newMessage.setUserBySenderId(sender);
			newMessage.setUserByRecivedId(reciver);
			newMessage.setContent(message.getContent());
			newMessage.setDateTimeSent(message.getDateTimeSent());
			newMessage.setDateTimeRecived(message.getDateTimeRecived());
			
			messageRepository.save(newMessage);
			return new ResponseEntity<>(HttpStatus.CREATED);
		}
	}

	public List<MessageDto> getMessages(Integer firstAccountId, Integer secondAccountId) { 
		
		Account firstAccount = userRepository.findOne(firstAccountId);
		Account secondAccount = userRepository.findOne(secondAccountId);
		
		List<Message> messageList1 = messageRepository.findByUserBySenderIdAndUserByRecivedId(firstAccount, secondAccount);
		List<Message> messageList2 = messageRepository.findByUserBySenderIdAndUserByRecivedId(secondAccount, firstAccount);
		
		List<Message> conversation = new ArrayList<>(messageList1);
		conversation.addAll(messageList2);
		
		Collections.sort(conversation, new Comparator<Message>() {
			public int compare(Message o1, Message o2) {
				return o1.getDateTimeSent().compareTo(o2.getDateTimeSent());
			}
		});
		
		List<MessageDto> conversationDto = new ArrayList<>();

		for (Message msg : conversation) {
			conversationDto.add(msg.transferToDto());
		}
		return conversationDto;

	}
	
	
}
