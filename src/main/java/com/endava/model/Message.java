/**
 * 
 */
package com.endava.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.StringUtils;

import com.endava.dto.acount.AccountDto;
import com.endava.dto.message.MessageDto;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @author Stefan.Petrovic
 *
 */
@Entity
@Table(name = "message", catalog ="pokeme")
public class Message implements Serializable{
	
	@Id
	@GeneratedValue(strategy = IDENTITY )
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "senderId",nullable = false)
	private Account userBySenderId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "reciverId", nullable = false)
	private Account userByRecivedId;
	
	@Column(name = "content", columnDefinition="TEXT", nullable = false )
	private String content;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DateTimeSent", nullable = false, length = 19)
	private Date dateTimeSent;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DateTimeRecived", length = 19)
	private Date dateTimeRecived;
	
	public Message(){
		
	}

	

	public Message(Account userBySenderId, Account userByRecivedId, String content, Date dateTimeSent) {
		super();
		this.userBySenderId = userBySenderId;
		this.userByRecivedId = userByRecivedId;
		this.content = content;
		this.dateTimeSent = dateTimeSent;
	}


	public Message(Account userBySenderId, Account userByRecivedId, String content, Date dateTimeSent, Date dateTimeRecived) {
		super();
		this.userBySenderId = userBySenderId;
		this.userByRecivedId = userByRecivedId;
		this.content = content;
		this.dateTimeSent = dateTimeSent;
		this.dateTimeRecived = dateTimeRecived;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Account getUserBySenderId() {
		return userBySenderId;
	}

	public void setUserBySenderId(Account userBySenderId) {
		this.userBySenderId = userBySenderId;
	}

	public Account getUserByRecivedId() {
		return userByRecivedId;
	}

	public void setUserByRecivedId(Account userByRecivedId) {
		this.userByRecivedId = userByRecivedId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDateTimeSent() {
		return dateTimeSent;
	}

	public void setDateTimeSent(Date dateTimeSent) {
		this.dateTimeSent = dateTimeSent;
	}

	public Date getDateTimeRecived() {
		return dateTimeRecived;
	}

	public void setDateTimeRecived(Date dateTimeRecived) {
		this.dateTimeRecived = dateTimeRecived;
	}
	
	public MessageDto transferToDto(){
		
		MessageDto dto = new MessageDto();
		dto.setSenderId(userBySenderId.getId());
		dto.setRecivedId(userByRecivedId.getId());
		dto.setContent(content);
		dto.setDateTimeSent(dateTimeSent);
		dto.setDateTimeRecived(dateTimeRecived);
		
		return dto;
			
	}
	

/*	public void updateFromDto(MessageDto dto){
		this.setUserByRecivedId(dto.getUserByRecivedId());
		this.setUserBySenderId(dto.getUserBySenderId());
		this.setContent(dto.getContent());
		this.setDateTimeRecived(dto.getDateTimeRecived());
		this.setDateTimeSent(dto.getDateTimeSent());
	}*/

}
