/**
 * 
 */
package com.endava.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.endava.dto.message.MessageDto;
import com.endava.service.MessageService;

/**
 * @author Stefan.Petrovic
 *
 */

@RestController
public class MessageController {

	@Autowired
	private MessageService messageService;
	
	//create new message

	@RequestMapping(value ="/message", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody MessageDto dto){
		
	     messageService.createMessage(dto);
	     
	     return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value ="/conversation", method = RequestMethod.GET)
	public ResponseEntity<List<MessageDto>> getMessages (@RequestParam("firstId") Integer firstAccountId, @RequestParam("secondId") Integer secondAccountId){
		
		List<MessageDto> conversation = messageService.getMessages(firstAccountId, secondAccountId);
		
		return new ResponseEntity<List<MessageDto>>(conversation, HttpStatus.OK);
		
	}
	
}
