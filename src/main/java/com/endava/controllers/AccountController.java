package com.endava.controllers;

import java.util.List;

import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.HttpOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.endava.dto.acount.AccountDto;
import com.endava.dto.acount.AccountRegistrationDto;
import com.endava.dto.contacts.ContactStatusResponseDto;
import com.endava.dto.contacts.ContactsResponseDto;
import com.endava.dto.login.LoginRequestDto;
import com.endava.dto.login.LoginResponseDto;
import com.endava.dto.login.LogoutRequestDto;
import com.endava.exceptions.EmailOccupiedException;
import com.endava.exceptions.InvalidCredentialsException;
import com.endava.service.AccountService;




@RestController
public class AccountController {
	
	@Autowired
	private AccountService accountService;
	
	//create new user
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public ResponseEntity<?> createUser(@RequestBody AccountRegistrationDto user) throws EmailOccupiedException{
		
	    accountService.createAccount(user);
		System.out.println("akaunt je uspesno kreiran");
		
		return new ResponseEntity<>( HttpStatus.CREATED);
	}
	
	// user log in
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<LoginResponseDto> login(@RequestBody LoginRequestDto loginRegDto ) throws InvalidCredentialsException{
		
		LoginResponseDto loginResDto = accountService.validateLoginCredentials(loginRegDto);
		return new ResponseEntity<LoginResponseDto>(loginResDto , HttpStatus.OK);
	}
	
	// user log off
	@RequestMapping(value = "/logoff", method = RequestMethod.POST)
	public ResponseEntity<?> logout (@RequestBody LogoutRequestDto userIdToLogout){
		
		Integer id = userIdToLogout.getId();
		accountService.logout(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	// return all contacts of user
	@RequestMapping(value = "/contacts", method = RequestMethod.GET)
	public ResponseEntity<List<ContactsResponseDto>> getMyContacts(@RequestParam("id") Integer id){
		
		List<ContactsResponseDto> myContacts = accountService.getMyContacts(id);
		
		return new ResponseEntity<List<ContactsResponseDto>>(myContacts, HttpStatus.OK);
	}
	
	// return contact status
	@RequestMapping(value = "/contactStatus", method = RequestMethod.GET)
	public ResponseEntity<List<ContactStatusResponseDto>> getContactsStauts (@RequestParam("id") Integer id){
		
		List<ContactStatusResponseDto> contactStatus = accountService.getContactsStauts(id);
		
		return new ResponseEntity<List<ContactStatusResponseDto>>(contactStatus, HttpStatus.OK);
	}
	
	
	//________________________________________________________________________________________________
	
	//get all user
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<AccountDto>> getUsers(){
		List<AccountDto> users = accountService.getAll();
		return new ResponseEntity<List<AccountDto>>(users, HttpStatus.OK);
	}
	
	//get user
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<AccountDto> getUser(@PathVariable Integer id){
		AccountDto user = accountService.getUserById(id);
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<AccountDto>(user, HttpStatus.OK);
	}
	
	//delete user
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable Integer id){
		accountService.deleteUser(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	//update user
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public ResponseEntity<?> updateUser(@PathVariable Integer id, @RequestBody AccountDto user){
			user.setId(id);
			accountService.updateUser(user);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	

}
