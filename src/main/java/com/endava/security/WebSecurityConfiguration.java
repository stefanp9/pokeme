/**
 * 
 */
package com.endava.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.endava.model.Account;
import com.endava.repository.AccountRepository;

/**
 * @author Stefan.Petrovic
 *
 */
@Configuration
public class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter  {
	
	@Autowired
	AccountRepository accountRepository;
	
	@Override
	public void init(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService()).passwordEncoder(passwordencoder());
	}

	@Bean
	UserDetailsService userDetailsService() {
		return new UserDetailsService() {

			@Override
			public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
				Account account = accountRepository.findByEmail(email);

				if (account != null) {
					String role;

					if (account.isAdmin() == true)
						role = "ADMIN";
					else
						role = "USER";

					return new User(account.getEmail(), account.getPassword(), true, true, true, true,
							AuthorityUtils.createAuthorityList(role));
				} else {
					throw new UsernameNotFoundException("Could not find the user with an email '" + email + "'");
				}
			}
		};
	
}
	@Bean(name = "passwordEncoder")
	public PasswordEncoder passwordencoder() {
		return new BCryptPasswordEncoder();
	}
}

@EnableWebSecurity
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().anyRequest().permitAll().and().
		httpBasic().and().
		csrf().disable();
		// http.authorizeRequests().anyRequest().fullyAuthenticated().and().
		// httpBasic().and().
		// csrf().disable();

//		http.authorizeRequests().antMatchers("/signup").permitAll()
//								.antMatchers("/login").permitAll()
//								.antMatchers("/logout").permitAll()
//								.antMatchers("/{id}").permitAll()
//								.antMatchers("/delete").permitAll()
//								.anyRequest().authenticated().and()
//								.httpBasic().and().csrf()
//								.disable();
	}

}



