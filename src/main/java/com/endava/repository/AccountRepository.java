package com.endava.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.endava.model.Account;

public interface AccountRepository extends Repository<Account, Integer>{
	

	List<Account> findAll();
	
	Account findOne(Integer id);
	
	Account save(Account user);
	
	void delete(Account user);
	
	Account findByEmail(String email);
	
	List<Account> findByIdNot(Integer id);
	
	
	
}
