/**
 * 
 */
package com.endava.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.endava.model.Account;
import com.endava.model.Message;

/**
 * @author Stefan.Petrovic
 *
 */
public interface MessageRepository extends Repository<Message, Integer>{

	List<Message> findAll();

	Message findOne(Integer id);

	Message save(Message message);

	void delete(Message message);
	
	List<Message> findByUserBySenderIdAndUserByRecivedId(Account senderAccount, Account reciverAccount);
	
	
	
}
