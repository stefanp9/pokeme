/**
 * 
 */
package com.endava.dto.contacts;

/**
 * @author Stefan.Petrovic
 *
 */
public class ContactsResponseDto {

	private Integer id;
	private String name;
	
	public ContactsResponseDto() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
