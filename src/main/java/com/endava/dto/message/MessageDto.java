/**
 * 
 */
package com.endava.dto.message;

import java.util.Date;

import com.endava.model.Account;

/**
 * @author Stefan.Petrovic
 *
 */
public class MessageDto {
	
	private Integer senderId;
	private Integer recivedId;
	private String content;
	private Date dateTimeSent;
	private Date dateTimeRecived;
	
	
	
	public MessageDto() {

	}
	public MessageDto(Integer id, Integer senderId, Integer recivedId, String content, Date dateTimeSent,
			Date dateTimeRecived) {
		super();
		this.senderId = senderId;
		this.recivedId = recivedId;
		this.content = content;
		this.dateTimeSent = dateTimeSent;
		this.dateTimeRecived = dateTimeRecived;
	}

	public Integer getSenderId() {
		return senderId;
	}
	public void setSenderId(Integer senderId) {
		this.senderId = senderId;
	}
	public Integer getRecivedId() {
		return recivedId;
	}
	public void setRecivedId(Integer recivedId) {
		this.recivedId = recivedId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getDateTimeSent() {
		return dateTimeSent;
	}
	public void setDateTimeSent(Date dateTimeSent) {
		this.dateTimeSent = dateTimeSent;
	}
	public Date getDateTimeRecived() {
		return dateTimeRecived;
	}
	public void setDateTimeRecived(Date dateTimeRecived) {
		this.dateTimeRecived = dateTimeRecived;
	}
	
	

	
}
