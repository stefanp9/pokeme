/**
 * 
 */
package com.endava.dto.login;

/**
 * @author Stefan.Petrovic
 *
 */
public class LogoutRequestDto {

	private Integer id;

	public LogoutRequestDto() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
		

	

}
