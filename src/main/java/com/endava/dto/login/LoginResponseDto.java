/**
 * 
 */
package com.endava.dto.login;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Stefan.Petrovic
 *
 */
@JsonInclude(Include.NON_NULL)
public class LoginResponseDto {
	
	private String response;
	private Integer accountId;
	
	public LoginResponseDto (){
		
	}
	
	

	public LoginResponseDto(String response, Integer accountId) {
		super();
		this.response = response;
		this.accountId = accountId;
	}



	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Integer getAccountId() {
		return accountId;
	}

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	
	
	
	
}
